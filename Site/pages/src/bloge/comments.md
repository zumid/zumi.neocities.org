<!--:mdiocre-template="../../templates/built/blog.html"-->
<!--:title="Commenting is a thing apparently"-->
<!--:longdate="2019-09-18T22:03:05+07:00"-->
<!--:lastedited="2019-09-18T22:25:48+07:00"-->
<!--:date="2019-09-18"-->
# <!--:title-->
<time datetime="<!--:longdate-->" class="article-date"> <!--:date--> </time> <span class="last-edited meta">(**Last edited** <time datetime="<!--:lastedited-->" class="article-date"><!--:lastedited--></time>)</span>

So I found a commenting system similar to Disqus.. only less corporate-y and more iframe-y. The most Web 2.0ish name ever: Comntr.

Because of Chrome's security policies, this may not work in it. [Alternatively...](../..)

**Additional heads up** - the captcha won't work unless you add comntr.live to your security exceptions (outdated certificate, apparently)
