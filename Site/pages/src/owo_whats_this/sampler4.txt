{

"originalURL_thisisnotevenusedlol"	: "https://www.trylore.com/story/59605eaa6f372e1ce27c5362",

"title" :	"What if you fell in love with a Pokemon?",
"createdBy":	"seismicboss",

"characters":
[
	["Gold", "Mom"],
	["9E067A", "27069E"]
],
				
"dialogue":
[
			[0, "Mom, I have something to tell you."],
			[1, "Sure, what is it honey?"],
			[0, "I... You know how I caught Suicune?"],
			[1, "Yes! I'm so proud of you for that still."],
			[1, "There are a lot of League Champions, but not many people with Legendary Pokemon."],
			[0, "Right, no. I'm definitely... unique."],
			[0, "So. You know how Suicune can speak telepathically?"],
			[1, "Yes! I seem to remember him being very charming."],
			[0, "Yeah, I thought so too."],
			[0, "In fact... We really hit it off."],
			[1, "That's great!"],
			[0, "I mean... Really, really hit it off."],
			[1, "What... What do you mean?"],
			[0, "Mom..."],
			[0, "Suicune and I..."],
			[0, "We're in a relationship."],
			[1, "Excuse me?"],
			[0, "For almost a year now."],
			[0, "We've been dating."],
			[0, "We're in love."],
			[1, "..."],
			[1, "I'm sorry for the language, Gold, but..."],
			[1, "What the fuck?"],
			[0, "I know it sounds crazy! People and Pokemon."],
			[0, "Like a weird old man with a Kirlia."],
			[1, "Well, yes."],
			[0, "But it isn't like that! Suicune is Legendary. More intelligent than most humans."],
			[1, "Gold... I don't know what to say."],
			[1, "Do you... Have you..."],
			[0, "Mom, I don't want to talk about that."],
			[0, "Just trust me. We love each other."],
			[1, "I don't know, Gold. I love you, but I don't know how to feel about this."],
			[0, "I mean, what's the problem? If we're both happy, we both consent..."],
			[1, "Gold, please. I need some time. I can't think about this right now."],
			[0, "All right. I get it. I get it."],
			[0, "Have as much time as you want."],
			[1, "Thank you for telling me, I guess."],
			[0, "Yeah, I've wanted to for a long time."],
			[0, "Let me know if you're ready to talk more. Or if you have any questions."],
			[1, "I will, honey."],
			[1, "Oh, actually. Does this mean you're gay?"],
			[0, "SUICUNE IS GENDERLESS MOM, GOD"]
]

}