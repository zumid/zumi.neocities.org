# called from ..
# add the parent dir to the module search
import os
import sys
sys.path.append(os.path.abspath('../../MDiocre'))
###
import re
from mdiocre.core import MDiocre
from feedgen import feed
import string

# blog directory
blog_dir = os.path.join('src', 'bloge')

# feed info
fg = feed.FeedGenerator()
fg.title("Zumi's Personal Website")
fg.subtitle("I don't know what I'm doing here lol")
fg.logo('https://zumi.neocities.org/img/new/heading.jpeg')
fg.author( {'name':'Zumi Daxuya','email':'daxuya.zumi@protonmail.com'} )
fg.language('en')
fg.generator('python-feedgen (MDiocre v.3.0)')

# feed links
fg.link(href='https://zumi.neocities.org/bloge/')
fg.link(href='https://zumi.neocities.org/bloge/feed.rss', rel='self', type='application/rss+xml')

# set up MDiocre and file list
m = MDiocre()
blog_files = [i for i in os.listdir(blog_dir) \
              if i.lower()[-3:] == '.md' \
              and not os.path.splitext(i.lower())[0].startswith('index')]

# make entry for each
for f in blog_files:
	file_path = os.path.join(blog_dir, f)
	file_name, file_ext = os.path.splitext(f)
	
	# read file
	with open(file_path, 'r') as content:
		content_vars = m.process(content.read())
	
	# prepare feed entry
	fe = fg.add_entry()
	
	# set title
	if content_vars.get('title') != '':
		blog_title = content_vars.get('title')
	else:
		blog_title = file_name
	
	# set date
	blog_pub = content_vars.get("longdate")
	
	# clear datetime markup
	blog_content = re.sub(r'<p><time.+/time></p>|<p><time.+/time>\)</span></p>', '', content_vars.get('content'), count=1)
	
	# fill feed entry
	fe.title(blog_title)
	fe.description(blog_content)
	fe.link(href="https://zumi.neocities.org/bloge/{}.html".format(file_name))
	fe.published(blog_pub)

# print out the rss feed
print(fg.rss_str(pretty=True).decode(encoding='utf-8'))
