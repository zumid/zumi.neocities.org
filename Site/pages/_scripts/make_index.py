# called from ..
# add the parent dir to the module search
import os
import sys
sys.path.append(os.path.abspath('../../MDiocre'))
###
from summarizer import summarize
import re
from mdiocre.core import MDiocre
from feedgen import feed
import string
import datetime

# blog directory
blog_dir = os.path.join('src', 'bloge')
template_file = os.path.join('_scripts', 'index.template')
site_template = os.path.join('..', 'templates', 'built', 'blog.html')

# set up MDiocre and file list
m = MDiocre()
blog_files = [i for i in os.listdir(blog_dir) \
              if i.lower()[-3:] == '.md' \
              and not os.path.splitext(i.lower())[0].startswith('index')]

summaries = []

# make entry for each
for f in blog_files:	
	file_path = os.path.join(blog_dir, f)
	file_name, file_ext = os.path.splitext(f)
	
	# read file
	with open(file_path, 'r') as content:
		content_vars = m.process(content.read())
	
	# set title
	if content_vars.get('title') != '':
		blog_title = content_vars.get('title')
	else:
		blog_title = file_name
	
	# set short date
	blog_pub = content_vars.get("date")
	
	# clear *all* markup for plaintext
	text_content = re.sub(r'<p><time.+/time></p>|<p><time.+/time>\)</span></p>', '', content_vars.get('content'), count=1)
	text_content = re.sub(r'(<h[1-6]>).*?(</h[1-6]>)', '', text_content)
	text_content = re.sub('<.*?>', '', text_content)
	text_content = text_content.replace(blog_title, '', 1).strip()
	
	# attempt to make summary
	summarized = " ".join(summarize(blog_title, text_content, count=1)).replace('\n', ' ')
	
	# Fill out the index entry, this controls the formatting of the entry
	# I'm violating principles because I'm too lazy to just make a new style
	index_heading = "* **{0}** - [{1}]({2})".format(blog_pub, blog_title, '{}.html'.format(file_name))
	entry_string = '{0}</br><blockquote><small>{1}</small></blockquote>'.format(index_heading, summarized)
	
	# add to index
	summaries.append(entry_string)

# sort by descending
summaries.sort(key=lambda date: datetime.datetime.strptime(re.search(r'(\d{4}-\d{2}-\d{2})', date).groups()[0], "%Y-%m-%d"))
summaries.reverse()

# render inner template
with open(template_file, 'r') as template:
	index_vars = m.process('\n'.join(summaries))
	template_html = template.read()
	inner_content = m.render(template_html, index_vars)

# render final page
with open(site_template, 'r') as site_tpl:
	page_tpl = site_tpl.read()
	# this effectively puts it in the "content" variable of the outer
	# content
	outer_content = m.process(inner_content)
	
	# set title of index
	outer_content.assign('title = "Blog Index"')
	
	# output final index page
	print(m.render(page_tpl, outer_content))
